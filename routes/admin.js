const express = require("express");
const router = express.Router();
//const {getAll, run, getLastOrder, matriculaExistente, getLastId, obtenerTipoMedia} = require("../db/conexion");
//const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/images"});
//const fileUpload = upload.single('scr');
const uploadFields = upload.fields([
    { name: 'src_representativa', maxCount: 1 },
    { name: 'src_propia', maxCount: 1 }
]);
const session = require("express-session");
const AuthMiddleware = require('../middlewares/auth.middleware');
const AdminInicioController = require('../controllers/admin/inicio.controller');
const AdminIntegrantesController = require('../controllers/admin/integrantes.controller');
const AdminTipoMediaController = require('../controllers/admin/tipoMedia.controller');
const AdminMediaController = require('../controllers/admin/media.controller');
//Como prefijo /admin
//Todo para login 
router.use(session({
    name: "session",
    secret: "clave-aleatoria-y-secreta",
    resave: false,
    saveUninitialized: true
}));


router.use((req, res, next) => {
    console.log(req.session.userId); // Agrega esta línea para verificar el userId
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});

router.use(AuthMiddleware.validateAuth);


//INICIO
router.get("/",  AdminInicioController.index);

//INTEGRANTES
//Como prefijo /admin/integrantes/listar
router.get("/integrantes/listar", AdminIntegrantesController.index);
router.get("/integrantes/crear", AdminIntegrantesController.create);
router.post("/integrantes/crear", AdminIntegrantesController.store);
router.post("/integrantes/eliminar/:matricula", AdminIntegrantesController.destroy);
router.get("/integrantes/editar/:matricula", AdminIntegrantesController.edit);
router.post("/integrantes/update/:matricula", AdminIntegrantesController.update);

// TIPO MEDIA
//Como prefijo /admin/tipo_media/listar
router.get("/tipo_media/listar", AdminTipoMediaController.index);
router.get("/tipo_media/crear", AdminTipoMediaController.create);
router.post("/tipo_media/crear", AdminTipoMediaController.store);
router.get("/tipo_media/editar/:id", AdminTipoMediaController.edit);
router.post("/tipo_media/update/:id", AdminTipoMediaController.update);
router.post("/tipo_media/eliminar/:id", AdminTipoMediaController.destroy);

// MEDIA
//Como prefijo /admin/media/listar
router.get("/media/listar", AdminMediaController.index);
router.get("/media/crear", AdminMediaController.create);
router.post("/media/crear", uploadFields,AdminMediaController.store);
router.get("/media/editar/:id", AdminMediaController.edit);
router.post("/media/update/:id", uploadFields, AdminMediaController.update);
router.post("/media/eliminar/:id", AdminMediaController.destroy);


//exportar
module.exports = router;