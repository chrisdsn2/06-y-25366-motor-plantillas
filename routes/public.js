const express = require("express");
const router = express.Router();
//const {getAll} = require("../db/conexion");
require("dotenv").config();
const PaginasControllerPublic = require("../controllers/public/paginasController");

//......................Direcciones.............
router.use((req, res, next) => {
    res.locals.ENLACE=process.env.ENLACE;
    res.locals.NOMBRE=process.env.NOMBRE;
    res.locals.APELLIDO=process.env.APELLIDO;    
    res.locals.MATERIA=process.env.MATERIA;
    next();
});

router.get("/", PaginasControllerPublic.index);

router.get("/wordcloud/", PaginasControllerPublic.indexWordCloud);

router.get("/info_curso/", PaginasControllerPublic.indexCurso);

router.get("/:matricula", PaginasControllerPublic.indexIntegranteMatricula);

//exportamos al router
module.exports = router;
