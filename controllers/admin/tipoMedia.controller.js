//const { create } = require("hbs");
//const {getAll, run, getLastOrder, matriculaExistente, getLastId, obtenerTipoMedia, getOne, tipoMediaEnMedia} = require("../../db/conexion");
const tipoMediaStoreSchema = require("../../validators/tipoMedia/create.tipoMedia");
const tipoMediaUpdateSchema = require("../../validators/tipoMedia/edit.tipoMedia");
const TipoMediaModel = require("../../models/tipoMedia.model");
const MediaModel = require("../../models/media.model");

const TipoMediaController = {
    index: async function (req, res) { 
        try {
            const tipoMedia = await TipoMediaModel.getAll(req);
            console.log("Tipo Media obtenido con éxito:", tipoMedia); // Si hay error muestra en consola
            res.render("admin/tipo_media/index", {
                tipoMedia: tipoMedia,
            });
        } catch (err) {
            res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al obtener los tipos de media!'));
        }
    },

    create: async function (req, res) {
        res.render('admin/tipo_media/crearTipoMedia',{
            nombre: req.query.nombre || '',
            activo: req.query.activo || '1'  // Valor por defecto 'activo'
        });
    },

    store: async function (req, res) {
        
        const {error} = tipoMediaStoreSchema.validate(req.body);
        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent(errorMessages.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
        } else {
            try {
                await TipoMediaModel.create(req.body);
                res.redirect(`/admin/tipo_media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    },

    //update - método de editar un registro
    update: async function (req, res) {
        const {error} = tipoMediaUpdateSchema.validate(req.body);
        const id = parseInt(req.params.id);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/admin/tipo_media/editar/${id}?error=${encodeURIComponent(errorMessages.join(';'))}`);
        } else {
            try {
                await TipoMediaModel.update(req.body, id);
                res.redirect("/admin/tipo_media/listar?success=" + encodeURIComponent('¡Tipo media actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al actualizar el tipo media!'));
            }
        }
    },

    //edit - formulario de edición
    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const tipoMedia = await TipoMediaModel.getById(id);
            res.render("admin/tipo_media/editarFormTipoMedia", {
                tipoMedia: tipoMedia
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al obtener el tipo media!'));
        }
    },


    //destroy - operación de eliminar un registro
    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        const idAsociado = await MediaModel.getByField('media', 'tipoMedia', id);

        if (idAsociado) {
            res.redirect(`/admin/tipo_media/listar?error=${encodeURIComponent('¡No se puede eliminar el registro porque está siendo utilizado en la tabla media!')}`);
        } else {
            try {
                await TipoMediaModel.delete(id);
                res.redirect("/admin/tipo_media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/tipo_media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    }
}

module.exports = TipoMediaController;