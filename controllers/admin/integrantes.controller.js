const { create } = require("hbs");
//const {getAll} = require("../../db/conexion");
const {getAll, run, getLastOrder, matriculaExistente,matriculaEnMedia, getOne} = require("../../db/conexion");
const integranteStoreSchema = require("../../validators/integrantes/create.integrante");
const integranteUpdateSchema = require ("../../validators/integrantes/edit.integrante")
const IntegranteModel = require("../../models/integrante.model");
/*index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro*/


//index - listado /admin/integrantes/listar
const IntegrantesController = {
    index: async function (req, res) {
        try {
            const integrantes = await IntegranteModel.getAll(req);
            res.render("admin/integrantes/index", {
                integrantes: integrantes,
            });
        } catch (err) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('Error al obtener los integrantes'))
        }
    },

//create - formulario de creación
    create: function (req, res) {
        res.render('admin/integrantes/crearFormIntegrantes', {
            matricula: req.query.matricula || '',
            nombre: req.query.nombre || '',
            apellido: req.query.apellido || ''
        });
    },

//store - método de guardar en la base de datos
    store: async function (req, res) {
        const {error} = integranteStoreSchema.validate(req.body);
        console.log(req.body);
        if (error) {
            const mensajesError = error.details.map(detail => detail.message);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(mensajesError.join(';'))}&matricula=${encodeURIComponent(req.body.matricula)}&nombre=${encodeURIComponent(req.body.nombre)}&apellido=${encodeURIComponent(req.body.apellido)}&codigo=${encodeURIComponent(req.body.codigo)}`);
        } else {
            try {
                await IntegranteModel.create(req.body);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('Integrante creado correctamente')}`);
            } catch (err) {
                console.log(err);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('Error al insertar el registro')}`);
            }
        }
    },

    //show - formulario de ver un registor

    //update - método de editar un registro
    update: async function (req, res) {
        const {error} = integranteUpdateSchema.validate(req.body);
        const matricula = req.params.matricula;
        
        if (error) {
            const mensajesError = error.details.map(detail => detail.message);
            res.redirect(`/admin/integrantes/editar/${matricula}?error=${encodeURIComponent(mensajesError.join(';'))}`);
        } else {
            try {
                await IntegranteModel.update(req.body, matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Integrante actualizado correctamente!'));
            } catch (err) {
                console.log(err);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al actualizar al integrante!'));
            }
        }
    },

    //edit - formulario de edición
    edit: async function (req, res) {
        try {
            const matricula = req.params.matricula;
            const integrante = await IntegranteModel.getById(matricula);
            res.render("admin/integrantes/editarFormIntegrantes", {
                integrante: integrante
            });
        } catch (err) {
            console.log(err);
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al obtener al integrante!'));
        }
    },
    //destroy - operación de eliminar un registro
    destroy: async function (req, res) {
        const matricula = req.params.matricula;

        if (await matriculaEnMedia(matricula)) {
            return res.redirect(`/admin/integrantes/listar?error=${encodeURIComponent('¡No se puede eliminar el integrante porque tiene media asociada!')}`); 
        }else{
            try{
                await IntegranteModel.delete(matricula);
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Registro eliminado correctamente!')}`);
        } catch (err) {

          console.error(err);
          res.redirect(`/admin/integrantes/listar?error=${encodeURIComponent('¡Error al eliminar el registro!')}`);
        }
        }
      }
};




module.exports = IntegrantesController;