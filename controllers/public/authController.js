const loginSchema = require("../../validators/auth/login");
const AuthModel = require("../../models/auth.model");
const bcrypt = require("bcrypt");

const AuthController = {
    loginForm: (req, res) => {
        res.render("login/login");
    },

    login: async (req, res) => {
        const {error} = loginSchema.validate(req.body);

        if (error) {
            const errorMessages = error.details.map(detail => detail.message);
            res.redirect(`/auth/login?error=${encodeURIComponent(errorMessages.join(';'))}`);
        } else {
            const {email, contrasenha} = req.body;
            try {
                const user = await AuthModel.getUserByEmail(email);
                

                if (user && await bcrypt.compare(contrasenha, user.contrasenha)) {
                    req.session.userId = user.id;
                    console.log('Session userId set:', req.session.userId); // Verifica que se establezca correctamente
                    res.redirect(`/admin?success=¡Sesión iniciada correctamente, bienvenido!`);
                } else {
                    res.redirect(`/auth/login?error=Usuario o contraseña incorrectos`);
                }
            } catch (error) {
                console.error('Error during login:', error);  // Agrega esta línea para imprimir el error
                res.redirect(`/auth/login?error=Error al iniciar sesión`);
            }
        }
    },

    logout: (req, res) => {
        req.session.destroy(() => {
            res.redirect("/?success=Sesión cerrada correctamente");
        });
    }
}

module.exports = AuthController;
