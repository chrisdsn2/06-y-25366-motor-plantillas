const {getIntegrante, getAllMatriculas, getByMatricula} = require("../../models/integrante.model");
const {getTipoMedia} = require("../../models/tipoMedia.model");
const {getMediaByMatricula} = require("../../models/media.model");

const PaginasController = {
    index: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
    
        try {
            //const rows = await getAll(sql); // Asegúrate de que getAll esté correctamente implementado
            res.render("index", {
                integrantes: primerNombreIntegrantes,
                showAdminLink: true, // Se habilita Admin solo dentro de la página de inicio
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).send("Error Interno del Servidor");
        }
    },

    indexWordCloud: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("wordcloud", {
            integrantes: primerNombreIntegrantes
        });
    },
  
    indexCurso: async (req, res) => {
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
        res.render("info_curso", {
            integrantes: primerNombreIntegrantes
        });
    },

    indexIntegranteMatricula: async (req, res, next) => {
        const matriculas = (await getAllMatriculas()).map(obj => obj.matricula);
        const tipoMedia = await getTipoMedia();
        const matricula = req.params.matricula;
        const integrantes = await getIntegrante();
        const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getByMatricula(matricula);
            const mediaFilter = await getMediaByMatricula(matricula);
            res.render('integrantes', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: primerNombreIntegrantes
            });
        } else {
            next();
        }
    }
}

// Función para mostrar solo el primer nombre de los integrantes
function obtenerPrimerNombre(integrantes) {
    return integrantes.map(integrante => {
        const primerNombre = integrante.nombre.split(" ")[0];
        return {...integrante, nombre: primerNombre};
    })
}

module.exports = PaginasController;