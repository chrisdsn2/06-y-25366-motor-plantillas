const Joi = require('joi');

const integranteStoreSchema = Joi.object({
    matricula: Joi.string()
        .required()
        .messages({
            'string.empty': '¡La matrícula no puede estar vacía!',
    }),


    nombre: Joi.string()
    .max(50)
    .required()
    .messages({
        'string.empty': '¡El nombre no puede estar vacío!',
        'string.max': '¡El nombre debe tener una longitud máxima de 50 caracteres!',
    }),

    apellido:
    Joi.string()
        .max(50)
        .required()
        .messages({
            'string.empty': '¡El apellido no puede estar vacío!',
            'string.max': '¡El apellido debe tener una longitud máxima de 50 caracteres!',
        }),

    codigo:
        Joi.string()
            .max(50)
            .required()
            .messages({
                'string.empty': '¡El codigo no puede estar vacío!',
                'string.max': '¡El codigo debe tener una longitud máxima de 50 caracteres!',
            }),

    activo:
    Joi.required()
        .messages({
            'any.required': '¡Debe seleccionar un estado (activo o inactivo)!',
        }),
}).options({abortEarly: false});

module.exports = integranteStoreSchema;