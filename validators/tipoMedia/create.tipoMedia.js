
const Joi = require('joi');

const tipoMediaStoreSchema = Joi.object({
    nombre:
        Joi.string()
        .max(50)
        .required()
        .messages({
            'string.empty': '¡El nombre no puede estar vacío!',
            'string.max': '¡El nombre debe tener una longitud máxima de 50 caracteres!',
        }),

    activo:
        Joi.required()
        .messages({
            'any.required': '¡Debe seleccionar un estado (activo o inactivo)!',
        }),
}).options({abortEarly: false});

module.exports = tipoMediaStoreSchema;