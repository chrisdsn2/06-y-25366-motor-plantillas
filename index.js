// importar express
const express = require("express");
const hbs = require("hbs");

const bodyParser = require("body-parser");//para manejar los datos del formulario enviados por POST.


const db = require("./db/conexion");

require('dotenv').config(); //nav.example
//requerimos la conexion a la base de datos
//creamos la aplicacion express
const app = express();
const path = require('path');
//importamos los recursos para sesiones
const session = require("express-session");
//importamos archivos de rutas
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');//para los tipo admin
const routerAuth = require('./routes/auth');//para los tipo login

//Body Parser false por hbs para manejar formularios
app.use(bodyParser.urlencoded({ extended:false}));
// Middleware para manejo de errores
/* app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Hubo un Error algo salio mal!');
}); */
app.use(session({
    name: "session",
    secret: "clave-aleatoria-y-secreta",
    resave: false,
    saveUninitialized: true,

    
    cookie: {
        secure: false, // Cambia a true si usas HTTPS
        maxAge: 24 * 60 * 60 * 1000 // Duración de la cookie en milisegundos
    }
}));

//Incluir la ruta dentro del proyecto express
app.use("/", router);
app.use("/admin", routerAdmin);//llama para los tipo administracion

app.use("/auth", routerAuth);// llama para los usuarios login

// recuperar archivos estáticos desde la carpeta "public"
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');

// se configura que se apunta a la carpeta views
app.set("views", __dirname + "/views");

//para el navegacion generica
hbs.registerPartials(__dirname + "/views/partials");

// Helper para comparar valores y seleccionar la opción correcta en un select
//Middleware eq
hbs.registerHelper('eq', function(a, b) {
    return a === b;
});

app.use((req, res, next) => {
    res.locals.isAuthenticated = !!req.session.userId;
    next();
});


// iniciar app escuchando puerto parametro
const puerto = process.env.PORT ||3000;

app.listen(puerto, () => {
    console.log("Servidor corriendo en el puerto"+puerto);
});

//Se utiliza consola para que se imprima una notificacion
//console.log("base de datos simulada", db);
//console.log(db.integrantes);
//console.log(db.integrantes[0].codigo);