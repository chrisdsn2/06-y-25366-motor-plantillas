const {db, getLastOrder, getLastId} = require("../db/conexion");

    const TipoMediaModel = {
        async getAll(req){
            let query = "SELECT * FROM tipoMedia WHERE activo = 1";
            let queryParams = [];
        
        for (const prop in req.query["s"]) {
            if (req.query["s"][prop]) {
                query += ` AND ${prop} LIKE?`;
                queryParams.push(`%${req.query["s"][prop]}%`);
            }
        }

        query += " ORDER BY orden";
        return new Promise((resolve, reject) => {
            db.all(query, queryParams, (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                    console.log("tipoMedia",tipoMedia)
                }
            });
        });
    },

    // Obtener un registro por ID
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM tipoMedia WHERE id = ?`, [id], (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },

     // Crear registro
     async create(req) {
        const lastOrder = await getLastOrder('tipoMedia');
        const newOrder = lastOrder + 1;
        const lastId = await getLastId('tipoMedia');
        const newId = lastId + 1;

        return new Promise((resolve, reject) => {
            db.run("insert into tipoMedia (id, nombre, orden, activo) values (?, ?, ?, ?)",[newId,  req.nombre, newOrder, req.activo], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

     // Actualizar registro
     update(req, id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET nombre = ? WHERE id = ?", [req.nombre, id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Borrar registro
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    // Borrar registro
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },
    
    getTipoMedia() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre", (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },
}

module.exports = TipoMediaModel;