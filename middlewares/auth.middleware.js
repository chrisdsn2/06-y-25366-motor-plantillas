const AuthMiddleware = {
    validateAuth: (req, res, next) => {
        if (req.session.userId) {
            console.log('User is authenticated.');
            next();
        } else {
            console.log('No user session found.');
            res.redirect('/auth/login');
        }
    }
}

module.exports = AuthMiddleware;
